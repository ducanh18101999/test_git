/* eslint-disable react-hooks/exhaustive-deps */
import { List, Modal, Tabs } from "antd";
import { useEffect } from "react";
import callApi from "../../api/api";
import HTTP_METHOD from "../../api/method";
import { useState } from "react";

function ModalChiTiet({ show, closePreview, id }) {
  const [data, setData] = useState({});
  const [img, setImg] = useState();
  useEffect(() => {
    show &&
      callApi({
        url: `/tai-khoan/lich-su-giao-dich/${id}`,
        method: HTTP_METHOD.GET,
      }).then((res) => {
        setData(res.order);
        setImg(res.order.productImg[0]);
      });
  }, [show]);
  return (
    <>
      <Modal
        title="Thông tin đơn hàng"
        open={show}
        onCancel={closePreview}
        width={800}
        footer={[null]}
      >
        <>
          <div className="titleOder">Tên sản phẩm: {data.productName}</div>
          <img style={{ width: "200px", height: "200px" }} src={img} alt="" />
          <div>
            sizeVn: {data.sizeVn} | sizeUs:{data.sizeUs} | sizeCm:
            {data.sizeUs}
          </div>
          <div className="titleOder">{data.totalPrice}</div>
          <div className="titleOder">{data.receiverName}</div>
          <div className="titleOder">{data.receiverPhone}</div>
          <div className="titleOder">{data.receiverAddress}</div>
          <div className="titleOder">Tình trạng thái: {data.statusText}</div>
        </>
      </Modal>
    </>
  );
}

export default function LichSuDatHang() {
  const [data1, setData1] = useState([]);
  const [data2, setData2] = useState([]);
  const [data3, setData3] = useState([]);
  const [data4, setData4] = useState([]);
  const [data5, setData5] = useState([]);
  const [id, setId] = useState(0);
  const [show, setShow] = useState(0);
  useEffect(() => {
    callApi({
      url: "/tai-khoan/lich-su-giao-dich",
      method: HTTP_METHOD.GET,
    }).then((res) => {
      setData1(res.orderList.filter((i) => +i.status === 1));
      setData2(res.orderList.filter((i) => +i.status === 2));
      setData3(res.orderList.filter((i) => +i.status === 3));
      setData4(res.orderList.filter((i) => +i.status === 4));
      setData5(res.orderList.filter((i) => +i.status === 5));
    });
  }, []);
  const menu = [
    {
      key: 1,
      label: <h3>Chờ lấy hàng</h3>,
      children: (
        <List
          itemLayout="horizontal"
          dataSource={data1}
          renderItem={(item, index) => (
            <List.Item
              style={{ border: "1px solid #dddddd", cursor: "pointer" }}
              onClick={() => {
                setShow(true);
                setId(item.id);
              }}
            >
              <div>
                {" "}
                <span style={{ color: "blue" }}>#{item.id}</span>
                <div style={{ display: "flex", justifyContent: "center" }}>
                  <img
                    style={{ width: "150px", height: "200px" }}
                    src={item.productImg[0]}
                    alt=""
                  />
                  <div
                    style={{
                      margin: "0 15px",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                    }}
                  >
                    <div>
                      <b>{item.productName}</b>
                    </div>
                    <div>
                      sizeVn: {item.sizeVn} | sizeUs:{item.sizeUs} | sizeCm:
                      {item.sizeUs}
                    </div>
                  </div>
                </div>
              </div>
            </List.Item>
          )}
        />
      ),
    },
    {
      key: 2,
      label: <h3>Đang giao hàng</h3>,
      children: (
        <List
          itemLayout="horizontal"
          dataSource={data2}
          renderItem={(item, index) => (
            <List.Item
              style={{ border: "1px solid #dddddd", cursor: "pointer" }}
              onClick={() => {
                setShow(true);
                setId(item.id);
              }}
            >
              <div>
                {" "}
                <span style={{ color: "blue" }}>#{item.id}</span>
                <div style={{ display: "flex", justifyContent: "center" }}>
                  <img
                    style={{ width: "150px", height: "200px" }}
                    src={item.productImg[0]}
                    alt=""
                  />
                  <div
                    style={{
                      margin: "0 15px",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                    }}
                  >
                    <div>
                      <b>{item.productName}</b>
                    </div>
                    <div>
                      sizeVn: {item.sizeVn} | sizeUs:{item.sizeUs} | sizeCm:
                      {item.sizeUs}
                    </div>
                  </div>
                </div>
              </div>
            </List.Item>
          )}
        />
      ),
    },
    {
      key: 3,
      label: <h3>Đã giao hàng</h3>,
      children: (
        <List
          itemLayout="horizontal"
          dataSource={data3}
          renderItem={(item, index) => (
            <List.Item
              style={{ border: "1px solid #dddddd", cursor: "pointer" }}
              onClick={() => {
                setShow(true);
                setId(item.id);
              }}
            >
              <div>
                {" "}
                <span style={{ color: "blue" }}>#{item.id}</span>
                <div style={{ display: "flex", justifyContent: "center" }}>
                  <img
                    style={{ width: "150px", height: "200px" }}
                    src={item.productImg[0]}
                    alt=""
                  />
                  <div
                    style={{
                      margin: "0 15px",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                    }}
                  >
                    <div>
                      <b>{item.productName}</b>
                    </div>
                    <div>
                      sizeVn: {item.sizeVn} | sizeUs:{item.sizeUs} | sizeCm:
                      {item.sizeUs}
                    </div>
                  </div>
                </div>
              </div>
            </List.Item>
          )}
        />
      ),
    },
    {
      key: 4,
      label: <h3>Đơn hàng trả lại</h3>,
      children: (
        <List
          itemLayout="horizontal"
          dataSource={data4}
          renderItem={(item, index) => (
            <List.Item
              style={{ border: "1px solid #dddddd", cursor: "pointer" }}
              onClick={() => {
                setShow(true);
                setId(item.id);
              }}
            >
              <div>
                {" "}
                <span style={{ color: "blue" }}>#{item.id}</span>
                <div style={{ display: "flex", justifyContent: "center" }}>
                  <img
                    style={{ width: "150px", height: "200px" }}
                    src={item.productImg[0]}
                    alt=""
                  />
                  <div
                    style={{
                      margin: "0 15px",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                    }}
                  >
                    <div>
                      <b>{item.productName}</b>
                    </div>
                    <div>
                      sizeVn: {item.sizeVn} | sizeUs:{item.sizeUs} | sizeCm:
                      {item.sizeUs}
                    </div>
                  </div>
                </div>
              </div>
            </List.Item>
          )}
        />
      ),
    },
    {
      key: 5,
      label: <h3>Đơn hàng bị hủy</h3>,
      children: (
        <List
          itemLayout="horizontal"
          dataSource={data5}
          renderItem={(item, index) => (
            <List.Item
              style={{ border: "1px solid #dddddd", cursor: "pointer" }}
              onClick={() => {
                setShow(true);
                setId(item.id);
              }}
            >
              <div>
                {" "}
                <span style={{ color: "blue" }}>#{item.id}</span>
                <div style={{ display: "flex", justifyContent: "center" }}>
                  <img
                    style={{ width: "150px", height: "200px" }}
                    src={item.productImg[0]}
                    alt=""
                  />
                  <div
                    style={{
                      margin: "0 15px",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                    }}
                  >
                    <div>
                      <b>{item.productName}</b>
                    </div>
                    <div>
                      sizeVn: {item.sizeVn} | sizeUs:{item.sizeUs} | sizeCm:
                      {item.sizeUs}
                    </div>
                  </div>
                </div>
              </div>
            </List.Item>
          )}
        />
      ),
    },
  ];

  return (
    <>
      <div
        style={{
          padding: "50px 300px",
          overflow: "auto",
          maxHeight: "80vh",
        }}
      >
        <Tabs defaultActiveKey="1" items={menu} />
      </div>
      {show && (
        <ModalChiTiet id={id} show={show} closePreview={() => setShow(false)} />
      )}
    </>
  );
}
