import { PlusOutlined } from "@ant-design/icons";
import { Button, Pagination, Table } from "antd";
import { useState } from "react";
import { converDate } from "../../../../assets/comonFc";
import { useEffect } from "react";
import callApi from "../../../../api/api";
import HTTP_METHOD from "../../../../api/method";
import { cloneDeep } from "lodash";

export default function DanhSachDonHang() {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(0);
  const [total, setTotal] = useState(1);
  const [dataView, setDataView] = useState([]);

  useEffect(() => {
    getData();
  }, []);
  function getData() {
    callApi({
      url: "/admin/orders",
      method: HTTP_METHOD.GET,
    }).then((res) => {
      setData(res);
      setPage(1);
      setTotal(res.length);
    });
  }
  useEffect(() => {
    const newData = cloneDeep(data).slice((page - 1) * 5, (page - 1) * 5 + 5);
    setDataView(newData);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, data]);
  const status = [
    { key: 1, value: "Chờ lấy hàng", style: "#ed9c28" },
    { key: 2, value: "Đang giao hàng", style: "#cccccc" },
    { key: 3, value: "Đã giao hàng", style: "#47a447" },
    { key: 4, value: "Đơn hàng bị trả lại", style: "#d2322d" },
    { key: 5, value: "Đơn hàng bị hủy", style: "#000000" },
  ];
  const columns = [
    {
      title: "Mã đươn hàng",
      dataIndex: "id",
      key: "id",
      alight: "right",
    },
    {
      title: "Người nhận",
      dataIndex: "nguoiNhan",
      key: "tenDm",
      alight: "right",
    },
    {
      title: "Điện thoại",
      dataIndex: "dienThoai",
      key: "tenDm",
      alight: "right",
    },
    {
      title: "Trạng thái",
      dataIndex: "trangThai",
      key: "statusDm",
      alight: "right",
      with: 80,
      render: (val) => {
        const dataRow = status.find((i) => +i.key === +val);
        return (
          <span style={{ color: "white", background: dataRow.style }}>
            {dataRow.value}
          </span>
        );
      },
    },
    {
      title: "Sản phẩm",
      dataIndex: "sanPham",
      key: "sanPham",
      alight: "right",
    },
    {
      title: "Ngày tạo",
      dataIndex: "ngayTao",
      key: "ngayTao",
      alight: "right",
      render: (val) => (val ? converDate(val) : ""),
    },
    {
      title: "Ngày sửa",
      dataIndex: "ngaySua",
      key: "ngaySua",
      alight: "right",
      render: (val) => (val ? converDate(val) : ""),
    },
  ];
  return (
    <>
      <div style={{ background: "white", padding: "15px" }}>
        <div className="titleTable" style={{ marginBottom: 20 }}>
          <b style={{ fontSize: "17px" }}>Danh sách danh mục</b>
        </div>
        <div className="listThaotac">
          <Button
            type="primary"
            style={{
              marginBottom: 16,
              marginRight: 20,
            }}
            icon={<PlusOutlined />}
          >
            Thêm mới danh mục
          </Button>
        </div>
        <Table
          columns={columns}
          dataSource={dataView}
          bordered
          pagination={false}
        />
        <div
          className="pagiantion"
          style={{ textAlign: "center", marginTop: "15px" }}
        >
          <Pagination
            onChange={(e) => setPage(e)}
            current={page}
            total={total}
            pageSize={5}
          />
        </div>
      </div>
    </>
  );
}
